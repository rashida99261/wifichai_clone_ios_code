//
//  vendorBaseViewController.swift
//  WifiChai
//
//  Created by MacbookPro on 09/07/18.
//  Copyright © 2018 mavencluster. All rights reserved.
//

import UIKit

class vendorBaseViewController: UIViewController , SlideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        
            switch(index){
            case 0:
                self.openViewControllerBasedOnIdentifier("VendorHomeVC")
                break
            case 1:
                 self.openViewControllerBasedOnIdentifier("vendorOrderHistryVC")
                break
            case 2:
                self.openViewControllerBasedOnIdentifier("vendorBookingVC")
                break
            case 3:
                self.openViewControllerBasedOnIdentifier("VendorStoreVC")
                break
            case 4:
                self.openViewControllerBasedOnIdentifier("vendorNotificationVC")
                break
            case 5:
                self.openViewControllerBasedOnIdentifier("vendorProfileViewController")
                break
            case 6:
                self.openViewControllerBasedOnIdentifier("VendorSettingVC")
                break
            case 7:
                self.openViewControllerBasedOnIdentifier("vendorReferEarnVC")
                break
            case 8:
                
                let alert = UIAlertController(title: "", message: "Are you sure you want to exit?", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                    
                    userDef.set("", forKey: "session")
                    
                    let mainStoryboard = AppStoryboard.Main.instance
                    let login = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.present(login, animated: true, completion: nil)
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action) in
                }
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
                
                
                break
            default:
                print("default\n", terminator: "")
            }
    }
        
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        
        let mainStoryboard = AppStoryboard.Vendor.instance
        let destViewController : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: strIdentifier)
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(destViewController, animated: false, completion: nil)
        
    }
    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    
    func onSlideVendorMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        
        let mainStoryboard = AppStoryboard.Vendor.instance
        let menuVC  = mainStoryboard.instantiateViewController(withIdentifier: "vendorMenuVC") as! vendorMenuVC
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
    }
}
