import UIKit
import SDWebImage
class vendorBookingDetailVC: UIViewController {
    
    @IBOutlet weak var tblDetailBooking : UITableView!
    
    var dictDetails : NSDictionary = [:]
    var slotList : NSMutableArray = []
    
    @IBOutlet weak var lblBookingId : UILabel!
    @IBOutlet weak var lblStartDate : UILabel!
    @IBOutlet weak var lblEndDate : UILabel!
    @IBOutlet weak var lblSlotCount : UILabel!
    @IBOutlet weak var lblTotalAmt : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    
    
    @IBOutlet weak var imgCustomer : UIImageView!
    @IBOutlet weak var lblCustomerName : UILabel!
    @IBOutlet weak var lblCustomerNumber : UILabel!
    @IBOutlet weak var lblCustomerAddress : UILabel!
 //   @IBOutlet var ratingCustomer : [UIImageView]!
    
    @IBOutlet weak var btnDeny : UIButton!
    @IBOutlet weak var btnAccept : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblBookingId.text = "Booking Id : \((dictDetails["booking_id"] as? String)!)"
        lblEndDate.text = "End Date: \((dictDetails["end_date"] as? String)!)"
        lblStartDate.text = "Start Date: \((dictDetails["start_date"] as? String)!)"
        lblTotalAmt.text = "Total Amount: \((dictDetails["total_amount"] as? Int)!)"
        
        let booking_status = (dictDetails["booking_status"] as? String)!
        if(booking_status=="1"){
            lblStatus.text="Initialised"
            
            btnCancel.isHidden=true
            btnDeny.isHidden=false
            btnAccept.isHidden=false
            
        }else if(booking_status=="2"){
            lblStatus.text="Assing"
            
            btnCancel.isHidden=true
            btnDeny.isHidden=false
            btnAccept.isHidden=false
            
        }else if(booking_status=="3"){
            lblStatus.text="Accept"
            btnCancel.isHidden=false
            btnDeny.isHidden=true
            btnAccept.isHidden=true
            
            
        }else if(booking_status=="4"){
            lblStatus.text="Deny"
            
            btnCancel.isHidden=true
            btnDeny.isHidden=true
            btnAccept.isHidden=true
            
        }else if(booking_status=="5"){
            lblStatus.text="Cancel"
            
            btnCancel.isHidden=true
            btnDeny.isHidden=false
            btnAccept.isHidden=false
        }
        
        let slotArr = dictDetails["list_of_slots"] as! [NSDictionary]
        lblSlotCount.text = "Number of slots : \(slotArr.count)"
        
        let arr = dictDetails["list_of_slots"] as! NSArray
        
        for i in 0..<(arr.count){
            if let dict = arr[i] as? NSDictionary {
                self.slotList.add(dict)
            }
            
        }
        
        let dictCustmr = dictDetails["customer_details"] as! NSDictionary
        
        let img=dictCustmr["profile_pic"] as? String
        imgCustomer.setIndicatorStyle(.gray)
        imgCustomer.setShowActivityIndicator(true)
        if let imageURL = URL(string:"\(img!)") {
            imgCustomer.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
        }
        
        lblCustomerName.text=dictCustmr["name"] as? String
        lblCustomerNumber.text=dictCustmr["phone"] as? String
        lblCustomerAddress.text=dictCustmr["address"] as? String
        
        
//        let dictVendor = dictDetails["vendor_details"] as! NSDictionary
//        let rating=dictVendor["ratings"] as! NSNumber
//        let myInt = (rating).doubleValue
//        ApiResponse.callRating(myInt, starRating: ratingCustomer)
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clikcONBackbtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callAcceptDenyApi(strStatus : String){
        
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        let bookingId = (dictDetails["booking_id"] as? String)!
        
        let param = ["vendor_id" : "\(userid)","booking_id":"\(bookingId)","status":"\(strStatus)"] as NSDictionary
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.vendor_booking_acceptdeny_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                             self.dismiss(animated: true, completion: nil)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    self.hideHUD()
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
                
            }
        }
    }
    
    
    @IBAction func clickOnAccptBtn(_ sender : UIButton){
        self.callAcceptDenyApi(strStatus: "3")
    }
    
    @IBAction func clickOnRejectBtn(_ sender : UIButton){
        self.callAcceptDenyApi(strStatus: "4")
    }
    
    @IBAction func clickOnCancelBtn(_ sender : UIButton){
        self.callAcceptDenyApi(strStatus: "5")
    }

}

extension vendorBookingDetailVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return slotList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblDetailBooking.dequeueReusableCell(withIdentifier: "detailBookTblCell") as! detailBookTblCell
        let dict = self.slotList[indexPath.section] as! NSDictionary
        cell.lblCofeeCount.text = "Coffee Count: \((dict["no_of_coffee"] as? String)!)"
        cell.lblTeaCount.text = "Tea Count : \((dict["no_of_tea"] as? String)!)"
        cell.lblTime.text = "Time : \((dict["time"] as? String)!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(section==0){
             return "Slot List"
        }
        else{
            return ""
        }
    }
}

class detailBookTblCell : UITableViewCell{
    
    @IBOutlet weak var lblCofeeCount : UILabel!
    @IBOutlet weak var lblTeaCount : UILabel!
    @IBOutlet weak var lblTime : UILabel!
}
