import UIKit

class vendorBookingVC: vendorBaseViewController {
    
    @IBOutlet weak var tblBooking : UITableView!
    var arrBookingList : NSMutableArray = []
    var reversedArr : [NSDictionary] = []
    @IBOutlet weak var lblStatus : UILabel!
    
    @IBOutlet weak var switchEnableBooking : UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()

        tblBooking.tableFooterView=UIView()
        
        tblBooking.isHidden=true
        self.lblStatus.isHidden=true
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let docVerified = Userdict["is_document_verified"] as! Bool
        if(docVerified==true){
            switchEnableBooking.isEnabled=true
            
        }else{
            switchEnableBooking.isEnabled=false
        }
        
       
        
          switchEnableBooking.addTarget(self, action: #selector(stateChanged(switchState:)), for: UIControlEvents.valueChanged)
        // Do any additional setup after loading the view.
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let switchValue = userDef.value(forKey: "switchState") as? String
        if (switchValue == "1") {
            self.callBookingListApi()
            switchEnableBooking.isOn = true
        }
        else {
            switchEnableBooking.isOn = false
            self.callBookingListApi()
        }
    }
    
    @objc func stateChanged(switchState: UISwitch) {
        if switchState.isOn {
            self.callWantBookingApi(strWant_booking: true)
        } else {
            self.callWantBookingApi(strWant_booking: false)
            
            
        }
    }

    func callWantBookingApi(strWant_booking : Bool){
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)","want_booking":strWant_booking] as NSDictionary
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.vendor_wantBooking_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        let resp = dict["response"] as! NSDictionary
                        print(resp)
                        let wntBook = resp["want_booking"] as? Bool
                        print(wntBook!)
                        if(wntBook==true){
                            let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                                
                                self.callBookingListApi()
                                userDef.set("1", forKey: "switchState")
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                                self.callBookingListApi()
                                userDef.set("0", forKey: "switchState")
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                       ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
        

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideVendorMenuButtonPressed(sender)
    }
    
    func callBookingListApi(){
        
        showHud()
        self.arrBookingList = []
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.booking_list_vendor_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        
                   let res = dict["response"] as! NSDictionary
                        if let result = res["booking_list"] as? NSArray {
                            for i in 0..<(result.count){
                                if let dict = result[i] as? NSDictionary {
                                    self.arrBookingList.add(dict)
                                }
                            }
                            self.reversedArr = self.arrBookingList.reversed() as! [NSDictionary]
                            if(self.reversedArr.count==0){
                                self.lblStatus.isHidden=false
                                self.tblBooking.isHidden=true
                                
                            }else{
                                self.lblStatus.isHidden=true
                                self.tblBooking.isHidden=false
                                self.tblBooking.reloadData()
                            }
                            
                       
                    }
                        
                        print(self.arrBookingList)
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    

}

extension vendorBookingVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return reversedArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblBooking.dequeueReusableCell(withIdentifier: "bookingTblCell") as! bookingTblCell
        
        let dict = reversedArr[indexPath.section]
        cell.lblBookingId.text = "Booking Id : \((dict["booking_id"] as? String)!)"
        cell.lblToDate.text = "To: \((dict["end_date"] as? String)!)"
        cell.lblFromDate.text = "From: \((dict["start_date"] as? String)!)"
        
        let slotArr = dict["list_of_slots"] as! [NSDictionary]
        cell.lblSlotsCount.text = "Number of slots : \(slotArr.count)"
        
        cell.btnDEtailView.tag=indexPath.section
        cell.btnDEtailView.addTarget(self, action: #selector(clickOnDetailBtn(_:)), for: .touchUpInside)
        cell.btnDEtailView.addTopBorderWithColor(color: .lightGray, width: 1.5)
        
        return cell
    }
    
    func clickOnDetailBtn(_ sender : UIButton){
        let dict = reversedArr[sender.tag] 
        
        let vendorStory = AppStoryboard.Vendor.instance
        let modalViewController = vendorStory.instantiateViewController(withIdentifier: "vendorBookingDetailVC") as! vendorBookingDetailVC
        modalViewController.dictDetails=dict
        self.present(modalViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1.0)
        UIView.animate(withDuration: 0.5, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        }, completion: nil)
    }
}

class bookingTblCell : UITableViewCell{
    
    @IBOutlet weak var lblBookingId : UILabel!
    @IBOutlet weak var lblToDate : UILabel!
    @IBOutlet weak var lblFromDate : UILabel!
    @IBOutlet weak var lblSlotsCount : UILabel!
    
    @IBOutlet weak var btnDEtailView : UIButton!
    
}

//Accept deny cancel button hide show when status update.
