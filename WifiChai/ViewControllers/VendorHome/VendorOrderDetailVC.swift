

import UIKit

class VendorOrderDetailVC: UIViewController {
    
    @IBOutlet weak var tblDetails : UITableView!
    var arrItems_List : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
       
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickOnCrossBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    

    

}

extension VendorOrderDetailVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItems_List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tblDetails.dequeueReusableCell(withIdentifier: "detailTableCell") as! detailTableCell
        
        let dict = arrItems_List[indexPath.row]
        cell.lblItemNAme.text=dict["name"] as? String
        cell.lblItemQuantity.text=dict["quantity"] as? String
        
        

        if((indexPath.row) % 2 == 0){
            cell.backgroundColor=UIColorFromHex(rgbValue: 0xafffffff, alpha: 1.0)
        }else{
            cell.backgroundColor=UIColorFromHex(rgbValue: 0xdedede, alpha: 1.0)
        }
        
            return cell
    }
}

class detailTableCell: UITableViewCell {
    
    @IBOutlet weak var lblItemNAme : UILabel!
    @IBOutlet weak var lblItemQuantity : UILabel!
    
}
