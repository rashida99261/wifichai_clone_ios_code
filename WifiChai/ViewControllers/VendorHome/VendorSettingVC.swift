
import UIKit
var strChangeState : String = "0"
class VendorSettingVC: vendorBaseViewController {
    
    @IBOutlet weak var btnOnline_Offline : UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        
        if(strChangeState=="0"){
            self.btnOnline_Offline.backgroundColor=UIColor.lightGray
            self.btnOnline_Offline.setTitle("Offline", for: .normal)
        }else{
            self.btnOnline_Offline.backgroundColor=self.UIColorFromHex(rgbValue: 0x008E2A, alpha: 1.0)
            self.btnOnline_Offline.setTitle("Online", for: .normal)
        }
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnChangeLAngBtn(_ sender : UIButton){
        
        let mainStoryboard = AppStoryboard.Vendor.instance
        let chngeLAng = mainStoryboard.instantiateViewController(withIdentifier: "changeLangVC") as! changeLangVC
        self.present(chngeLAng, animated: true, completion: nil)
        
    }
    
    @IBAction func clickOnDocumentationBtn(_ sender : UIButton){
        
        let mainStoryboard = AppStoryboard.Vendor.instance
        let chngeLAng = mainStoryboard.instantiateViewController(withIdentifier: "vendorDocumentationVC") as! vendorDocumentationVC
        self.present(chngeLAng, animated: true, completion: nil)
        
    }
    
    @IBAction func clickOnChangePassBtn(_ sender : UIButton){
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let mobile = Userdict["mobile"] as! String
        let mainStory = AppStoryboard.Main.instance
        let changePass = mainStory.instantiateViewController(withIdentifier: "ChangePassViewController") as! ChangePassViewController
        changePass.strMobileNo = mobile
        self.present(changePass, animated: true, completion: nil)
        
        
    }
    
    @IBAction func clickOnCustomerServiceBtn(_ sender : UIButton){
        
        let mainStory = AppStoryboard.Main.instance
        let service = mainStory.instantiateViewController(withIdentifier: "CustomerServiceVC") as! CustomerServiceVC
        self.present(service, animated: true, completion: nil)
    }
    
    @IBAction func clickOnSecurityDepositBtn(_ sender : UIButton){
        let mainStory = AppStoryboard.Vendor.instance
        let service = mainStory.instantiateViewController(withIdentifier: "vendorSecurityDepositVC") as! vendorSecurityDepositVC
        self.present(service, animated: true, completion: nil)
    }
    
    @IBAction func clickOnOnlineBtn(_ sender : UIButton){
//        if(sender.tag==0){
//
//            self.callOnlineOfflineBtn(tag: 0)
//            sender.tag=1
//        }
//        else if(sender.tag==1){
//            self.callOnlineOfflineBtn(tag: 1)
//            sender.tag=0
//        }
        self.callOnlineOfflineBtn()
//        if(strChangeState=="0"){
//
//        }else{
//             self.callOnlineOfflineBtn()
//        }
    }
    
    func callOnlineOfflineBtn(){
        
        showHud()
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        var param = NSDictionary()
        
        if(strChangeState=="0"){
            param = ["vendor_id" : "\(userid)","online_offline_status" : true] as NSDictionary
        }else{
            param = ["vendor_id" : "\(userid)","online_offline_status" : false] as NSDictionary
        }
        
//        if(tag==0){
//            param = ["vendor_id" : "\(userid)","online_offline_status" : false] as NSDictionary
//        }else if(tag==1){
//             param = ["vendor_id" : "\(userid)","online_offline_status" : true] as NSDictionary
//        }
        
        
        
        ApiResponse.onResponsePost(url: Constant.v_Online_Ofline_Status_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                           strChangeState="1"
                        
                            self.btnOnline_Offline.backgroundColor=self.UIColorFromHex(rgbValue: 0x008E2A, alpha: 1.0)
                            self.btnOnline_Offline.setTitle("Online", for: .normal)
                     
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        strChangeState="0"
                        self.btnOnline_Offline.backgroundColor=UIColor.lightGray
                        self.btnOnline_Offline.setTitle("Offline", for: .normal)
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                
                OperationQueue.main.addOperation {
                    self.hideHUD()
                    
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                    
                }
                
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideVendorMenuButtonPressed(sender)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    
}
