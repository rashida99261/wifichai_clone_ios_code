
import UIKit

class vendorReferEarnVC: vendorBaseViewController {
    
    @IBOutlet weak var tblReferenceList : UITableView!
    
    @IBOutlet weak var lblwallet_amount : UILabel!
    @IBOutlet weak var lblrefer_code : UILabel!
    @IBOutlet weak var lblreward_amount : UILabel!
    
    @IBOutlet weak var viewNoReferral : UIView!
    
    var arrRefer_List : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblReferenceList.tableFooterView=UIView()
        DispatchQueue.main.async {
            self.callReferandEanApi()
        }
        
        viewNoReferral.isHidden=true
        tblReferenceList.isHidden=true
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
      
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideVendorMenuButtonPressed(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickOnInviteFriend(_ sender : UIButton){
        let textToShare = self.lblrefer_code.text!
        let activityVC = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        activityVC.excludedActivityTypes = [.print, .copyToPasteboard, .assignToContact, .saveToCameraRoll, .airDrop]
        DispatchQueue.main.async(execute: {
            self.present(activityVC, animated: true)
        })
    }
    
    func callReferandEanApi(){
        showHud()
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        let param = ["user_id" : "\(userid)"] as NSDictionary
        ApiResponse.onResponsePost(url: Constant.refer_earn_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        let responseDict = dict["response"] as! NSDictionary
                        print(responseDict)
                        
                        let strRefer = responseDict.object(forKey: "refer_code") as? String
                        self.lblrefer_code.text = "Your code : \(strRefer!)"
                        
                        
                        let strWallet = responseDict.object(forKey: "wallet_amount") as? String
                        self.lblwallet_amount.text = "INR : \(strWallet!)"
                        
                        
                        let reward_amount = responseDict.object(forKey: "reward_amount") as? String
                        
                        self.lblreward_amount.text = "Refer your friends and you both earn INR \(reward_amount!) when your friend sign up."
                        
                        
                        self.arrRefer_List = responseDict.object(forKey: "history") as! [NSDictionary]
                        
                        if(self.arrRefer_List.count==0){
                            self.viewNoReferral.isHidden=false
                            self.tblReferenceList.isHidden=true
                            
                        }else{
                            self.viewNoReferral.isHidden=true
                            self.tblReferenceList.isHidden=false
                            self.tblReferenceList.reloadData()
                        }
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
}
extension vendorReferEarnVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if(arrRefer_List.count==0){
                return 1
            }else{
                return arrRefer_List.count
            }
        }else if(section == 1){
            return 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblReferenceList.dequeueReusableCell(withIdentifier: "vendor_referTblCell") as! vendor_referTblCell
        
        if(arrRefer_List.count==0){
           
        }else{
           
            let dict = arrRefer_List[indexPath.row]
            print(dict)
            cell.lblRefferal.text=dict["referral_name"] as? String
            cell.lblAmount.text="INR : \((dict["reward_amount"] as? String)!)"
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tblReferenceList.dequeueReusableCell(withIdentifier: "vendor_HeaderTblCell") as! vendor_HeaderTblCell
        headerCell.headerLabel.text = "YOUR REFERALS";
        return headerCell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1.0)
        UIView.animate(withDuration: 0.5, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        }, completion: nil)
    }
}

class vendor_referTblCell : UITableViewCell{
    @IBOutlet weak var lblRefferal : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
}

class vendor_HeaderTblCell : UITableViewCell{
    
    @IBOutlet weak var headerLabel : UILabel!
}

