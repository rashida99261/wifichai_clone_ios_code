
import UIKit

class changeLangVC: UIViewController {
    
    
    @IBOutlet weak var btnEnglish : UIButton!
    @IBOutlet weak var btnHindi : UIButton!
    
    var selectLanguage : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        selectLanguage="English"
        // Do any additional setup after loading the view.
        
        UIApplication.shared.statusBarView?.backgroundColor = .black

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickOnChooseLanguage(_ sender : UIButton){
        
        if(sender.tag==0){
            
            btnEnglish.setImage(UIImage(named: "checkRadio"), for: .normal)
            btnHindi.setImage(UIImage(named: "unChechRadio"), for: .normal)
            selectLanguage="English"
            
        }else if(sender.tag==1){
            
            btnHindi.setImage(UIImage(named: "checkRadio"), for: .normal)
            btnEnglish.setImage(UIImage(named: "unChechRadio"), for: .normal)
            selectLanguage="Hindi"
        }
        
    }
    
    
    @IBAction func clicknBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func clickOnChangeBtn(_ sender : UIButton){
        
        if(selectLanguage=="Hindi"){
            L012Localizer.DoTheSwizzling()
            LocalizedClass.setAppleLAnguageTo(lang: "hi-IN")
            currentLang = "1"
            
           // let appdelegate = UIApplication.shared.delegate as! AppDelegate
           // appdelegate.add111()
        }else if(selectLanguage=="English"){
            L012Localizer.DoTheSwizzling()
            LocalizedClass.setAppleLAnguageTo(lang: "en")
            currentLang = "0"
            
          //  let appdelegate = UIApplication.shared.delegate as! AppDelegate
            //appdelegate.add111()
        }
        
//        if LocalizedClass.currentAppleLanguage() == "en" {
//            LocalizedClass.setAppleLAnguageTo(lang: "hi-IN")
//        } else {
//            LocalizedClass.setAppleLAnguageTo(lang: "en")
//        }
        
        
//        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//
//        let mainStoryboard = AppStoryboard.Main.instance
//        rootviewcontroller.rootViewController = mainStoryboard.instantiateViewController(withIdentifier: "CorporateCustHomeVC")
//         let mainwindow = (UIApplication.shared.delegate?.window!)!
//         UIView.transition(with: mainwindow, duration: 4, options: .transitionFlipFromBottom, animations: nil)
        
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        let _ = appDelegate?.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        
    }
    

  
}
//https://stackoverflow.com/questions/14335848/exit-application-when-click-button-ios
