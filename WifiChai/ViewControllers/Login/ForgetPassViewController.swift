

import UIKit

class ForgetPassViewController: UIViewController {
    
    @IBOutlet weak var txtMobileNumber : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarView?.backgroundColor = .black

        // Do any additional setup after loading the view.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickOnDismissBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnSendOtp(_ sender : UIButton){
        let phoneno = txtMobileNumber.text?.isPhoneNumber
        
        if((txtMobileNumber.text == "") || (txtMobileNumber.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Mobile number.", controller: self)
        }
        else if(phoneno==false){
            ApiResponse.alert(title: "Alert", message: "Mobile number should be of 10 digit.", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            self.callSendOtpApi()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
    }
    

    func callSendOtpApi(){
        let param = ["mobile" : "\(txtMobileNumber.text!)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.sendOTP_URL, parms: param) { (dict, error) in
            if(error == ""){
                print(dict)
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                       
                        let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                            
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "ChangePassViewController") as! ChangePassViewController
                            home.strMobileNo = self.txtMobileNumber.text!
                            self.present(home, animated: true, completion: nil)
                            

                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }

}
