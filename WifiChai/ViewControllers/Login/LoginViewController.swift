

import UIKit
import FBSDKLoginKit
import GoogleSignIn




class LoginViewController: UIViewController {
    
    @IBOutlet weak var lblSignUp : UILabel!
    @IBOutlet weak var viewSignUp : UIView!
    
    @IBOutlet weak var txtMobileNumber : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtMobileNumber.text = "8319746936"
        txtPassword.text = "1234567"
        
        
        //corpoarte login - id=9584303547 , pass=2345678
        //vendor login - id=8319746936 , pass=1234567
        //customer login - id=9977465092 , pass=1234567
        
        lblSignUp.text = "Sign Up"
        lblSignUp.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        lblSignUp.frame = CGRect(x: 0, y: 208, width: 50, height: 200)
        lblSignUp.textColor = UIColor.white
        lblSignUp.alpha = 1.0
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        viewSignUp.addGestureRecognizer(tap)
        viewSignUp.isUserInteractionEnabled = true
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func clickOnLoginBtn(_ sender : UIButton){
        
        let phoneno = txtMobileNumber.text?.isPhoneNumber
        
       
        if((txtMobileNumber.text == "") || (txtMobileNumber.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Mobile number.", controller: self)
            
        }
        else if((txtPassword.text == "") || (txtPassword.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Password.", controller: self)
        }
        else if(phoneno==false){
            ApiResponse.alert(title: "Alert", message: "Mobile number should be of 10 digit.", controller: self)
        }
        else if(!(ApiResponse.isPwdLenth(password: txtPassword.text!))){
            ApiResponse.alert(title: "Alert", message: "Password should be minimum of 7 digit.", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            showHud()
            self.callLoginApi()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
        
    }
    
    func callLoginApi(){
        
        
        let param = ["mobile" : "\(txtMobileNumber.text!)","password" : "\(txtPassword.text!)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.Login_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        let responseDict = dict["response"] as! NSDictionary
                        print(responseDict)
                        userDef.set(responseDict, forKey: "userInfo")
                        
                        let strMobileVerify = responseDict["is_mobile_verified"] as! Bool
                        let strDocVerified = responseDict["is_document_verified"] as! Bool

                        if(strMobileVerify == false){
                            let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in

                                let verify = self.storyboard?.instantiateViewController(withIdentifier: "VerifyUserViewController") as! VerifyUserViewController
                                verify.strMobileNo = self.txtMobileNumber.text!
                                self.present(verify, animated: true, completion: nil)
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        else{
                            let userType = responseDict["user_type"] as! String
                            if(userType=="2"){
                                
                                userDef.set("login", forKey: "session")
                                
                                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                self.present(home, animated: true, completion: nil)
                                //navigate to home
                            }
                            else if(userType == "1"){
//                                if(strDocVerified == false){
//                                    let alert = UIAlertController(title: "", message: "Verify your documents", preferredStyle: UIAlertControllerStyle.alert)
//                                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
//
//                                        let mainStoryboard = AppStoryboard.Vendor.instance
//                                        let chngeLAng = mainStoryboard.instantiateViewController(withIdentifier: "vendorDocumentationVC") as! vendorDocumentationVC
//                                        self.present(chngeLAng, animated: true, completion: nil)
//                                    }
//                                    alert.addAction(okAction)
//                                    self.present(alert, animated: true, completion: nil)
//                                }else{
                                    //navigate to vendor home
                                    
                                    userDef.set("Vendor_login", forKey: "session")
                                    
                                       let mainStoryboard = AppStoryboard.Vendor.instance
                                    let vendorHome = mainStoryboard.instantiateViewController(withIdentifier: "VendorHomeVC") as! VendorHomeVC
                                    self.present(vendorHome, animated: true, completion: nil)
                                //}
                            }
                            else if(userType == "3"){
//                                if(strDocVerified == false){
//                                    let alert = UIAlertController(title: "", message: "Verify your documents", preferredStyle: UIAlertControllerStyle.alert)
//                                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
//
//                                        let verify = self.storyboard?.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
//                                        self.present(verify, animated: true, completion: nil)
//                                    }
//                                    alert.addAction(okAction)
//                                    self.present(alert, animated: true, completion: nil)
//                                }else{
                                
                                    userDef.set("Corporate_login", forKey: "session")
                                    
                                    let mainStoryboard = AppStoryboard.Main.instance
                                    let vendorHome = mainStoryboard.instantiateViewController(withIdentifier: "CorporateCustHomeVC") as! CorporateCustHomeVC
                                    self.present(vendorHome, animated: true, completion: nil)
                                    //navigate to corporate customer home
                              //  }
                                
                            }
                            else if(userType == "4"){
                                if(strDocVerified == false){
                                    
                                    //Navigate to admin vendor document screen
                                }else{
                                    //Navigate to admin vendor home screen
                                }
                                
                            }
                            else{
                                
                            }
                   
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                      self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    self.hideHUD()
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                    
                }
                
            }
        }
    }
    
    // function which is triggered when handleTap is called
    func handleTap(_ sender: UITapGestureRecognizer) {
        let transition = CATransition()
        transition.duration = 0.7
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        present(presentedVC, animated: false, completion: nil)
        
        
       // let nvc = UINavigationController(rootViewController: presentedVC)
      //  nvc.navigationController?.navigationBar.isHidden = true
        
    }
    
    
    //Mark : Navigate to forget password screen
    @IBAction func clickOnForgetPass(_ sender : UIButton){
      let forget = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPassViewController") as! ForgetPassViewController
        self.present(forget, animated: true, completion: nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark : Navigate to facebook Login
    @IBAction func clickOnFacebookBtn(_ sender : UIButton){
        self.loginWithFacebook()
    }
    
    //Mark : Facebook integration code
    func loginWithFacebook(){
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.web
        
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) {
            (result, error) in
            if (error == nil) {
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if (fbloginresult.grantedPermissions.contains("email")){
                        
                        let myFbToken = (FBSDKAccessToken.current().tokenString) as String
                           print(myFbToken)
                        print(FBSDKAccessToken.current().tokenString)
                        
                        
                        
                        self.getFBUserData(strFbToken: myFbToken)
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    func getFBUserData(strFbToken : String) {
        if ((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result,error) -> Void in
                if (error == nil){
                    print(result!)
                    let dict = result as! NSDictionary
                    let userId = dict.value(forKey:"id") as! String
                    print(userId)
                    
                    let email = dict.value(forKey: "email") as! String
                    let fname = dict.value(forKey: "first_name") as! String
                    let lname = dict.value(forKey: "last_name") as! String
                    
                    let fullname = "\(fname) \(lname)"
                    self.callSignUpBySocialMedia(strNAme: fullname, strEmail: email, strId: userId, strLoginType: "2")
                }
            })
        }
    }

    //Mark : Navigate to gmail Login
    @IBAction func clickOnGoogleBtn(_ sender : UIButton){
        GIDSignIn.sharedInstance().signIn()
    }
    

}

extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count && self.count == 10
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}

extension LoginViewController : GIDSignInUIDelegate,GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error == nil) {
            guard let _ = user else {
                print("Error: did not receive data")
                return
            }
            
            if (user.profile.email != nil){
                print(" email")
            } else {
                print("no email")
            }
            
            let fname = user.profile.givenName
            let lname = user.profile.familyName
            let email = user.profile.email
            let userId = user.userID
            
            let fullName = "\(fname!) \(lname!)"
            
            self.callSignUpBySocialMedia(strNAme: fullName, strEmail: email!, strId: userId!, strLoginType: "3")
            
      
           // GIDSignIn.sharedInstance().signOut()
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    
    func callSignUpBySocialMedia(strNAme : String , strEmail : String , strId : String , strLoginType : String){
        
        showHud()
        
        var param = NSDictionary()
        if(strLoginType=="3"){
              param = ["name" : "\(strNAme)","email":"\(strEmail)","login_type":"\(strLoginType)","fcm_token":"","google_id":"\(strId)"] as NSDictionary
        }
        else if(strLoginType=="2"){
             param = ["name" : "\(strNAme)","email":"\(strEmail)","login_type":"\(strLoginType)","fcm_token":"","facebook_id":"\(strId)"] as NSDictionary
        }
        
       
       
        
        print(param)
        
        // facebook signup
        //   , login_type
        
        
        ApiResponse.onResponsePost(url: Constant.signUp_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                

                print("dict---\(dict)")
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        let responseDict = dict["response"] as! NSDictionary
                        print(responseDict)
                        
                        userDef.set(responseDict, forKey: "userInfo")
                        
                        let mobileNo = responseDict["mobile"] as! String
                        
                        let strMobileVerify = responseDict["is_mobile_verified"] as! Bool
                        let strDocVerified = responseDict["is_document_verified"] as! Bool
                        
                        if(mobileNo==""){
                            let story = AppStoryboard.Main.instance
                            let addDetail = story.instantiateViewController(withIdentifier: "addDetailSocialLoginVC") as! addDetailSocialLoginVC
                            
                            let res = dict["response"] as! [String:Any]
                            addDetail.dictResponse=res
                            self.present(addDetail, animated: true, completion: nil)
                        }
                        else if(strMobileVerify == false){
                            let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in

                                let verify = self.storyboard?.instantiateViewController(withIdentifier: "VerifyUserViewController") as! VerifyUserViewController
                                verify.strMobileNo = mobileNo
                                self.present(verify, animated: true, completion: nil)
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        else{
                            let userType = responseDict["user_type"] as! String
                            if(userType=="2"){

                                userDef.set("login", forKey: "session")

                                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                self.present(home, animated: true, completion: nil)
                                //navigate to home
                            }
                            else if(userType == "1"){
                                if(strDocVerified == false){
                                    let alert = UIAlertController(title: "", message: "Verify your documents", preferredStyle: UIAlertControllerStyle.alert)
                                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in

                                        let mainStoryboard = AppStoryboard.Vendor.instance
                                        let chngeLAng = mainStoryboard.instantiateViewController(withIdentifier: "vendorDocumentationVC") as! vendorDocumentationVC
                                        self.present(chngeLAng, animated: true, completion: nil)
                                    }
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    //navigate to vendor home

                                    userDef.set("Vendor_login", forKey: "session")

                                    let mainStoryboard = AppStoryboard.Vendor.instance
                                    let vendorHome = mainStoryboard.instantiateViewController(withIdentifier: "VendorHomeVC") as! VendorHomeVC
                                    self.present(vendorHome, animated: true, completion: nil)
                                }
                            }
                            else if(userType == "3"){
                                if(strDocVerified == false){
                                    let alert = UIAlertController(title: "", message: "Verify your documents", preferredStyle: UIAlertControllerStyle.alert)
                                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in

                                        let verify = self.storyboard?.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
                                        self.present(verify, animated: true, completion: nil)
                                    }
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil)
                                }else{

                                    userDef.set("Corporate_login", forKey: "session")

                                    let mainStoryboard = AppStoryboard.Main.instance
                                    let vendorHome = mainStoryboard.instantiateViewController(withIdentifier: "CorporateCustHomeVC") as! CorporateCustHomeVC
                                    self.present(vendorHome, animated: true, completion: nil)
                                    //navigate to corporate customer home
                                }

                            }
                            else if(userType == "4"){
                                if(strDocVerified == false){

                                    //Navigate to admin vendor document screen
                                }else{
                                    //Navigate to admin vendor home screen
                                }

                            }
                            else{

                            }

                        }

                       
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
}

extension LoginViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        textField.backgroundColor = UIColor.clear
        textField.layer.borderColor = UIColor.white.cgColor
        textField.layer.borderWidth = 3.0

        textField.textColor=UIColor.white
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        textField.backgroundColor = UIColor.lightGray
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.borderWidth = 3.0
        
        textField.textColor=UIColor.black
        
        return true
    }
    
}
