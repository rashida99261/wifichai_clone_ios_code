
import UIKit
import GooglePlacePicker
import CoreLocation
import SDWebImage
import Photos

class ProfileViewController: BaseViewController , CLLocationManagerDelegate{

    var locationManager:CLLocationManager!
    
    @IBOutlet weak var profileImg : UIImageView!
    @IBOutlet weak var lblUserName : UILabel!
    
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtMobile : UITextField!
    
    @IBOutlet weak var lblAddress : UILabel!
    
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblMobile : UILabel!
    
    @IBOutlet weak var lblLineName : UILabel!
    @IBOutlet weak var lblLineEmail : UILabel!
    @IBOutlet weak var lblLineMobile : UILabel!
    
    var imgPicker = UIImagePickerController()
    var chosenImage = UIImage()
    
    var lat : Double = 0.0
    var long : Double = 0.0
    
    var asset : PHAsset!
    var imgName : String = ""
    var isFilenameEmpty : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.determineMyCurrentLocation()
        
        imgPicker.delegate=self
        
        let dict = userDef.object(forKey: "userInfo") as! NSDictionary
        lblUserName.text=dict["name"] as? String
        txtName.text=dict["name"] as? String
        txtEmail.text=dict["email"] as? String
        txtMobile.text=dict["mobile"] as? String
        lblAddress.text=dict["address"] as? String
        
        let img=dict["profile_pic"] as? String
        
        profileImg.setIndicatorStyle(.gray)
        profileImg.setShowActivityIndicator(true)
        if let imageURL = URL(string:"\(img!)") {
            profileImg.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
          
        }
        
        profileImg.layer.cornerRadius = profileImg.frame.size.width / 2
        profileImg.clipsToBounds=true
        
        txtName.becomeFirstResponder()
        
 
    
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        lblAddress.addGestureRecognizer(tap)
        lblAddress.isUserInteractionEnabled = true
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideMenuButtonPressed(sender)
    }
    
    // function which is triggered when handleTap is called
    func handleTap(_ sender: UITapGestureRecognizer) {
        let center = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        self.present(placePicker, animated: true, completion: nil)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func clickOnEditBtn(_ sender : UIButton){
        ApiResponse.CameraGallery(controller: self, imagePicker: imgPicker)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        locationManager.startUpdatingLocation()
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        lat=userLocation.coordinate.latitude
        long=userLocation.coordinate.longitude
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error.localizedDescription)")
    }
    
    
    @IBAction func clickOnUpdateBtn(_ sender : UIButton){
        
        self.callUpdateDetailsApi()
    }
    

 
    func callUpdateDetailsApi(){
        
       // user_id, name, email, mobile, address, latitude, longitude
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        let mobile = Userdict["mobile"] as! String
        
        let param = ["user_id" : "\(userid)","name":"\(txtName.text!)","email":"\(txtEmail.text!)","mobile":"\(mobile)","address":"\(lblAddress.text!)","latitude":"\(lat)","longitude":"\(long)"] as NSDictionary
        
        
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.updateProfile_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                print(dict)
                if (status==true){
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        let responseDict = dict["response"] as! NSDictionary
                        userDef.set(responseDict, forKey: "userInfo")
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    self.hideHUD()
                    
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                    
                }
                
            }
        }

        
    }
  

}

extension ProfileViewController : GMSPlacePickerViewControllerDelegate{
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
     
        
        self.lblAddress.text=place.formattedAddress!
        
        
        lat=place.coordinate.latitude
        long=place.coordinate.longitude
        
        print(place.coordinate.latitude)
       print(place.coordinate.longitude)
        
       // print("Place name \(place.name)")
        //print("Place address \(String(describing: place.formattedAddress))")
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
  
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
}

extension ProfileViewController : CropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        
        
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        //profileImg.contentMode = .scaleAspectFit //3
       //profileImg.image = chosenImage //4
        if(takePhoto == "camera"){

            let dict = userDef.value(forKey: "userInfo") as! NSDictionary
            print(dict)
            let userFname = (dict.object(forKey: "first_name") as! String)
            //let userLname=(dict.object(forKey: "last_name") as! String)

            let first2 = userFname.substring(to:userFname.index(userFname.startIndex, offsetBy: 2))
            print(first2)
            let userId=(dict.object(forKey: "id") as! String)

            let i = userDef.object(forKey: "index")
            if(i==nil)
            {
                imgName = "\(first2)\(userId)0"
                userDef.set("1", forKey: "index")
            }
            else{
                imgName = "\(first2)\(userId)\(String(describing: i!))"

                if var zz = i as? Int{
                    zz += 1
                    userDef.set(zz, forKey: "index")
                    print(userDef.object(forKey: "index")!)
                    //Do your stuff
                }
                else if let zz = i as? String{
                    if Int(zz) != nil{
                    }
                }
            }
        }
        else if(takePhoto == "gallery"){
            if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                asset = result.firstObject
            }
        }
        
        dismiss(animated:true, completion: nil)
        
        let controller = CropViewController()
        controller.delegate = self
        controller.image = chosenImage
        
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true, completion: nil)
        profileImg.image = image
        
        self.upDateImg(chooseImg: profileImg.image!)
        
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func upDateImg(chooseImg : UIImage){
        
        showHud()
        let url = NSURL(string: Constant.uploadProfileImg_URL)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var body = Data()
        
        

        
        var imgfieName : String = ""
        if (takePhoto == "Gallery") {
            if(asset==nil){
                imgfieName="FirstImg.png"
            }
            else{
                let fname = (asset.value(forKey: "filename"))!   //"test.png"
                print(fname)
                
                let words = (fname as! String).components(separatedBy: ".")
                let fnm=words[0]
                print(fnm)
                
                imgfieName="\(fnm).png"
            }
            
        }
        else {
            imgfieName="\(imgName).png"
        }
        
        print(imgfieName)
        let mimetype = "image/png"
        
        
        
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let parameters : [String : String] = ["user_id" : "\(userid)"]
        print(parameters)
        
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        
        let image_data = UIImageJPEGRepresentation(chooseImg, 0.5)
        //let image_data = UIImageJPEGRepresentation(chosenImage, 0.5)
        if(image_data == nil)
        {
            return
        }
        //define the data post parameter
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"profile_pic\"; filename=\"\(imgfieName)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            
            
            
            
            DispatchQueue.main.async {
                self.hideHUD()
                guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                    print("error")
                    return
                }
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("datataattaatat = \(dataString!)")
                
                
                
                let dd = try! JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                print("Profile img Response : \(dd)")
                let status = dd["status"] as! Bool
                let message = dd["message"] as! String
                if (status == false) {
                    ApiResponse.alert(title: "", message: message, controller: self)
                }
                else {
                    ApiResponse.alert(title: "", message: message, controller: self)
                    
                }
            }
        }
        task.resume()
    }
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
}

extension ProfileViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
      
        if(textField == txtName){
            lblName.textColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            lblLineName.backgroundColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        }
        else if(textField == txtEmail){
            lblEmail.textColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            lblLineEmail.backgroundColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        }
        else if(textField == txtMobile){
            lblMobile.textColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            lblLineMobile.backgroundColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txtName){
            lblName.textColor = UIColor.lightGray
            lblLineName.backgroundColor = UIColor.lightGray
        }
        else if(textField == txtEmail){
            lblEmail.textColor = UIColor.lightGray
            lblLineEmail.backgroundColor = UIColor.lightGray
        }
        else if(textField == txtMobile){
            lblMobile.textColor = UIColor.lightGray
            lblLineMobile.backgroundColor = UIColor.lightGray
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
