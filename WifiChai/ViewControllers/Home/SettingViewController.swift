

import UIKit

class SettingViewController: BaseViewController {
    
    @IBOutlet weak var btnDocuments : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dict = userDef.value(forKey: "userInfo") as! NSDictionary
        let usertype=dict["user_type"] as! String
        
        if(usertype=="2"){
            btnDocuments.isHidden=true
        }else if(usertype=="3"){
            btnDocuments.isHidden=false
        }
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideMenuButtonPressed(sender)
    }
    
    @IBAction func clickOnChangePassBtn(_ sender : UIButton){
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let mobile = Userdict["mobile"] as! String
        let changePass = self.storyboard?.instantiateViewController(withIdentifier: "ChangePassViewController") as! ChangePassViewController
        changePass.strMobileNo = mobile
        self.present(changePass, animated: true, completion: nil)
        
    }
    
    @IBAction func clickOnCustomerServiceBtn(_ sender : UIButton){
        let service = self.storyboard?.instantiateViewController(withIdentifier: "CustomerServiceVC") as! CustomerServiceVC
        self.present(service, animated: true, completion: nil)
    }
    
    @IBAction func clickOnDocumentsBtn(_ sender : UIButton){
        
        let mainStoryboard = AppStoryboard.Main.instance
        let chngeLAng = mainStoryboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
        self.present(chngeLAng, animated: true, completion: nil)
        
    }
    
    @IBAction func clickOnChangeLAngBtn(_ sender : UIButton){
        
        let mainStoryboard = AppStoryboard.Vendor.instance
        let chngeLAng = mainStoryboard.instantiateViewController(withIdentifier: "changeLangVC") as! changeLangVC
        self.present(chngeLAng, animated: true, completion: nil)
        
    }
    
    

}

