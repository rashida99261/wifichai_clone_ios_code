//
//  Api.swift
//  CallBackDemo
//
//  Created by Prashant Shinde on 3/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import Foundation
import UIKit

class ApiResponse {
    
    static func onResponsePost(url: String,parms: NSDictionary, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let url = NSURL(string:"\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
            
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        completion(["":""], Constant.Status_Not_200)
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    completion(["":""], "\(String(describing: error))")
                    return
                }
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                    print("result = \(String(describing: result))")
                    completion(result! , "")
                }
                catch
                {
                }
            }
            task.resume()
        }
        catch
        {
        }
    }
    
    static func onResponseGet(url: String, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        
        let url = NSURL(string:"\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        
        do {
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        completion(["":""], Constant.Status_Not_200)
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    completion(["":""], "\(String(describing: error))")
                    return
                }
                
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                    print("result = \(String(describing: result))")
                    completion(result! , "")
                }
                catch
                {
                }
            }
            task.resume()
        }
    }
    
    static func onResponsePostPhp(url: String,parms: String, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {

        var request = URLRequest(url: URL(string: "\(url)")!)
        request.httpMethod = "POST"
        let postString = parms
        //print("post string = \(postString)")
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(["":""], "\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                completion(["":""], Constant.Status_Not_200)
                return
            }
            
            if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any]
            {
                //print("parsed data = \(parsedData)")
                completion(parsedData as NSDictionary , "")
            }
            else
            {
                OperationQueue.main.addOperation {
                    //LoadingIndicatorView.hide()
                    
                    print("false")
                }
            }
        }
        task.resume()
    }
    
    static func onResponseGetPhp(url: String, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let url = NSURL(string:"\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "GET"
        
        do {
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        completion(["":""], Constant.Status_Not_200)
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    completion(["":""], "\(String(describing: error))")
                    return
                }
                do
                {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                    // print("result1 = \(String(describing: result))")
                    completion(result!, "")
                }
                catch
                {
                }
            }
            task.resume()
        }
    }
    
    static func load(url: URL, to localUrl: URL, completion: @escaping () -> ()) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        var request = URLRequest(url: url as URL)
//        var request = try! URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData)
        request.httpMethod = "GET"
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Success: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                    completion()
                } catch (let writeError) {
                    print("error writing file \(localUrl) : \(writeError)")
                }
                
            } else {
                print("Failure: %@", error?.localizedDescription);
            }
        }
        task.resume()
    }
    
    
    static func alert(title: String, message : String , controller: UIViewController)
    {
        OperationQueue.main.addOperation
            {
               
               
                let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                controller.present(alert, animated: true, completion: nil)
        }
    }
    
    static func CameraGallery(controller: UIViewController, imagePicker : UIImagePickerController)
    {
        let actionSheetController : UIAlertController = UIAlertController(title: "", message: "Option to select", preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
          //  print("Camera")
            
            takePhoto="Camera"
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                controller.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default)
        { action -> Void in
            //print("")
            
            takePhoto="Gallery"
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = false
                controller.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(galleryActionButton)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        controller.present(actionSheetController, animated: true, completion: nil)
    }
    
    static func resize(_ image: UIImage, maxHt : Float, maxWd : Float) -> UIImage
    {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = maxHt       //300.0
        let maxWidth: Float = maxWd        //400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        //var compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = UIImagePNGRepresentation(img)
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
    
    static func validateEmail(_ emailStr : String) -> Bool
    {
        let a = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" as String
        let emailTest = NSPredicate(format: "SELF MATCHES %@", a)
        return emailTest.evaluate(with: emailStr)
    }
    
    static func validate(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    static func isPwdLenth(password: String) -> Bool {
        if password.count >= 7{
            return true
        }else{
            return false
        }
    }
    
    static func isCheckPasswordLength(password: String , confirmPassword : String) -> Bool {
        if password.count <= 7 && confirmPassword.count <= 7{
            return true
        }else{
            return false
        }
    }
    
    static func calculateHeightForString(_ inString:String, sz : Float) -> CGFloat
    {
        let messageString = inString
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: CGFloat(sz))]
        
        //let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(sz))]
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: attributes)
        
        let rect:CGRect = attrString!.boundingRect(with: CGSize(width : 300, height : CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context:nil)   //hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize.height         //requredSize.height  //to include button's in your tableview
    }
    
    static func calculateHeight(_ inString:String, sz : Float, szWidth : Float) -> CGFloat
    {
        let messageString = inString
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: CGFloat(sz))]
            
            //[NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(sz))]
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: attributes)
        
        let rect:CGRect = attrString!.boundingRect(with: CGSize(width : CGFloat(szWidth), height : CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context:nil)   //hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize.height         //requredSize.height  //to include button's in your tableview
    }
    
    static func calculateSizeForString(_ inString:String, sz : Float) -> CGRect
    {
        let messageString = inString
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: CGFloat(sz))]
            
            //[NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(sz))]
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: attributes)
        
        let rect:CGRect = attrString!.boundingRect(with: CGSize(width : CGFloat.greatestFiniteMagnitude, height : CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context:nil )   //hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize         //requredSize.height  //to include button's in your tableview
    }
    static func calculateHeightForString(_ inString:String) -> CGFloat
    {
        let messageString = inString
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: CGFloat(14.0))]
            
            //[NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0)]
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: attributes)
        
        let rect:CGRect = attrString!.boundingRect(with: CGSize(width : 300.0, height : CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context:nil )   //hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize.height         //requredSize.height  //to include button's in your tableview
    }
    
    
    static func decorateTextField(_ textField:UITextField,keyBoardType:UIKeyboardType,placeHolder:String,isSecureText:Bool) {
        
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0).cgColor
        border.borderWidth = width
        
        textField.spellCheckingType = .no
        textField.isSecureTextEntry = isSecureText
        textField.placeholder = placeHolder
        textField.keyboardType = keyBoardType
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        
    }
    
    static func decorateLabel(_ label:UILabel) {
        
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0).cgColor
        border.borderWidth = width
        
        border.frame = CGRect(x: 0, y: label.frame.size.height - width, width:  label.frame.size.width, height: label.frame.size.height)
        
        label.layer.addSublayer(border)
        label.layer.masksToBounds = true
        
    }
    
    static func UndecorateTextField(_ textField:UITextField,keyBoardType:UIKeyboardType,placeHolder:String,isSecureText:Bool) {
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.borderWidth = width
        
        textField.spellCheckingType = .no
        textField.isSecureTextEntry = isSecureText
        textField.placeholder = placeHolder
        textField.keyboardType = keyBoardType
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        
    }
    
    static func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    static func callRating(_ ratingValue : Double, starRating : [UIImageView])
    {
        if(ratingValue == 0)
        {
            starRating[0].image = (UIImage (named: "emptystar"))
            starRating[1].image = (UIImage (named: "emptystar"))
            starRating[2].image = (UIImage (named: "emptystar"))
            starRating[3].image = (UIImage (named: "emptystar"))
            starRating[4].image = (UIImage (named: "emptystar"))
        }
        else if((ratingValue >= 1) && (ratingValue < 1.0))
        {
            starRating[0].image = (UIImage (named: "halfstar"))
            starRating[1].image = (UIImage (named: "emptystar"))
            starRating[2].image = (UIImage (named: "emptystar"))
            starRating[3].image = (UIImage (named: "emptystar"))
            starRating[4].image = (UIImage (named: "emptystar"))
        }
        else if((ratingValue >= 1.0) && (ratingValue < 1.5))
        {
            starRating[0].image = (UIImage (named: "fullstar"))
            starRating[1].image = (UIImage (named: "emptystar"))
            starRating[2].image = (UIImage (named: "emptystar"))
            starRating[3].image = (UIImage (named: "emptystar"))
            starRating[4].image = (UIImage (named: "emptystar"))
        }
        else if((ratingValue >= 1.5) && (ratingValue < 2.0))
        {
            starRating[0].image = (UIImage (named: "fullstar"))
            starRating[1].image = (UIImage (named: "halfstar"))
            starRating[2].image = (UIImage (named: "emptystar"))
            starRating[3].image = (UIImage (named: "emptystar"))
            starRating[4].image = (UIImage (named: "emptystar"))
        }
        else if((ratingValue >= 2.0) && (ratingValue < 2.5))
        {
            starRating[0].image = (UIImage (named: "fullstar"))
            starRating[1].image = (UIImage (named: "fullstar"))
            starRating[2].image = (UIImage (named: "emptystar"))
            starRating[3].image = (UIImage (named: "emptystar"))
            starRating[4].image = (UIImage (named: "emptystar"))
        }
        else if((ratingValue >= 2.5) && (ratingValue < 3.0))
        {
            starRating[0].image = (UIImage (named: "fullstar"))
            starRating[1].image = (UIImage (named: "fullstar"))
            starRating[2].image = (UIImage (named: "halfstar"))
            starRating[3].image = (UIImage (named: "emptystar"))
            starRating[4].image = (UIImage (named: "emptystar"))
            
        }
        else if((ratingValue >= 3.0) && (ratingValue < 3.5))
        {
            starRating[0].image = (UIImage (named: "fullstar"))
            starRating[1].image = (UIImage (named: "fullstar"))
            starRating[2].image = (UIImage (named: "fullstar"))
            starRating[3].image = (UIImage (named: "emptystar"))
            starRating[4].image = (UIImage (named: "emptystar"))
        }
        else if((ratingValue >= 3.5) && (ratingValue < 4.0))
        {
            starRating[0].image = (UIImage (named: "fullstar"))
            starRating[1].image = (UIImage (named: "fullstar"))
            starRating[2].image = (UIImage (named: "fullstar"))
            starRating[3].image = (UIImage (named: "halfstar"))
            starRating[4].image = (UIImage (named: "emptystar"))
        }
        else if((ratingValue >= 4.0) && (ratingValue < 4.5))
        {
            starRating[0].image = (UIImage (named: "fullstar"))
            starRating[1].image = (UIImage (named: "fullstar"))
            starRating[2].image = (UIImage (named: "fullstar"))
            starRating[3].image = (UIImage (named: "fullstar"))
            starRating[4].image = (UIImage (named: "emptystar"))
        }
        else if((ratingValue >= 4.5) && (ratingValue < 5.0))
        {
            starRating[0].image = (UIImage (named: "fullstar"))
            starRating[1].image = (UIImage (named: "fullstar"))
            starRating[2].image = (UIImage (named: "fullstar"))
            starRating[3].image = (UIImage (named: "fullstar"))
            starRating[4].image = (UIImage (named: "halfstar"))
        }
        else if((ratingValue == 5.0))
        {
            for i in 0...4
            {
                starRating[i].image = (UIImage (named: "fullstar"))
            }
        }
    }
    
    
    
}

extension DateFormatter {
    
    convenience init (format: String) {
        self.init()
        dateFormat = format
        locale = Locale.current
    }
}

extension String {
    
    func toDate (format: String) -> Date? {
        return DateFormatter(format: format).date(from: self)
    }
    
    func toDateString (inputFormat: String, outputFormat:String) -> String? {
        if let date = toDate(format: inputFormat) {
            return DateFormatter(format: outputFormat).string(from: date)
        }
        return nil
    }
}

extension UIImage {
    
//    func scaleImageToSize(newSize: CGSize) -> UIImage {
//        var scaledImageRect = CGRect.zero
//        
//        let aspectWidth = newSize.width/size.width
//        let aspectheight = newSize.height/size.height
//        
//        let aspectRatio = max(aspectWidth, aspectheight)
//        
//        scaledImageRect.size.width = size.width * aspectRatio;
//        scaledImageRect.size.height = size.height * aspectRatio;
//        scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0;
//        scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0;
//        
//        UIGraphicsBeginImageContext(newSize)
//        draw(in: scaledImageRect)
//        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        
//        return scaledImage!
//    }
    
   
}

