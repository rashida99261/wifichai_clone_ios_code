//
//  L012Localizer.swift
//  LocalizationPOC
//
//  Created by MacbookPro on 01/06/18.
//  Copyright © 2018 mavencluster. All rights reserved.
//

import UIKit

class L012Localizer: NSObject {

    class func DoTheSwizzling() {
        // 1
//        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: Selector(""), overrideSelector: Selector("specialLocalizedStringForKey:value:table:"))
        
        
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(key:value:table:)))
    }
}

extension Bundle {
    @objc func specialLocalizedStringForKey(key: String, value: String?, table tableName: String?) -> String {
        let currentLanguage = LocalizedClass.currentAppleLanguage()
        var bundle = Bundle()
//        if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj"){
//            bundle = Bundle(path: _path)!
//
//        } else {
//            let _path = Bundle.main.path(forResource: "Main", ofType: "lproj")
//            bundle = Bundle(path: _path!)!
//        }
        
       
        
        if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj"){
            bundle = Bundle(path: _path)!
        }
        
        
    
        //return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        return (bundle.specialLocalizedStringForKey(key: key, value: value, table: tableName))
    }
}

//extension String {
//    func localized(lang:String) ->String {
//
//        let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj")
//        let bundle = Bundle(path: path!)
//
//        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
//    }}

/// Exchange the implementation of two methods for the same Class
func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
    let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
    let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector)!;
    if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
        class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else {
        method_exchangeImplementations(origMethod, overrideMethod);
    }
}
